export const useToken = (initial: string) => useState('token', () => initial)
export const useLoading = () => useState('loading', () => false)
export const useModal = () => useState('modal', () => [])
export const useToast = () => useState('toast', () => new Map())
