import md5 from 'js-md5'
import { generateHashKey } from '~~/utils/hash'
import { useLoading } from '~~/common/shared-state'

export function useAuth (redirectPath = '/') {
  const { API_ORIGIN, BASE_URL } = useRuntimeConfig().public
  const { $sessionChannel } = useNuxtApp()
  const { url, setRedirect } = useRedirect()
  const loading = useLoading()

  function goLogin (redirect) {
    setRedirect(redirect ?? redirectPath)
    return navigateTo('/sign-in')
  }

  async function logout () {
    loading.value = true
    await useApi(
      '/api/auth/logout',
      { method: 'POST' }
    )
    loading.value = false

    // 廣播登出
    $sessionChannel.post({ action: 'LOGOUT' })

    window.location.reload()
  }

  async function login ({ email, password, redirect = true }) {
    const key = generateHashKey()
    const hashPwd = md5(key + md5(password))

    loading.value = true
    const { error } = await useApi(
      '/api/auth/login',
      {
        method: 'POST',
        body: {
          account: email,
          password: hashPwd,
          key
        }
      }
    )
    loading.value = false

    if (error.value) {
      throw error.value
    }

    // 廣播refresh
    $sessionChannel.post({ action: 'RELOAD' })

    // 導頁
    if (redirect) {
      // 沒有設定redirect-uri則導回首頁
      window.location.href = url.value || BASE_URL
    }
  }

  async function signup (form) {
    const hashPwd = md5(form.password)

    loading.value = true
    const { error } = await useApi(
      '/api/auth/signup',
      {
        method: 'POST',
        body: {
          name: form.name,
          email: form.email,
          otp: form.code,
          password: hashPwd
        }
      }
    )
    loading.value = false

    if (error.value) {
      throw error.value
    }
  }

  function oauth (type) {
    if (!url.value) {
      setRedirect('/')
    }

    // 第三方登入
    loading.value = true
    window.location.href = `${API_ORIGIN}/auth/${type}`
  }

  return {
    goLogin,
    login,
    logout,
    signup,
    oauth
  }
}
