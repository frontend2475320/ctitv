// interface Payload {
//   name: string;
//   action: 'GET' | 'SET';
//   data?: string | {}
// }

export default defineNuxtPlugin(app => {
  const isSupport = 'BroadcastChannel' in window
  const expiredTime = 15 * 60 * 1000 // 15 min
  const channel = isSupport ? initBroadcast(app) : null

  return {
    provide: {
      sessionChannel: {
        post: (payload) => {
          if (!isSupport) {
            console.warn('broadcast channel is not supported')
            return
          }

          channel.postMessage(payload)
        },
        set: (name, data) => {
          if (!(data && name)) throw new Error('Missing data to set.')
          if (typeof data === 'string') data = JSON.parse(data)
          data.expired_at = Date.now() + expiredTime
          window.sessionStorage.setItem(name, JSON.stringify(data))
        }
      }
    }
  }
})

function initBroadcast (app) {
  const channel = new BroadcastChannel('_SITE_CHANNEL')
  const actions = new Map([
    ['_RETRIEVE_SESSION', (payload) => {
      const data = window.sessionStorage.getItem(payload.name)
      if (!data) return

      const isExpired = Date.now() - JSON.parse(data).expired_at > 0
      app.$log(`${payload.name} expired: `, isExpired)

      if (isExpired) {
        window.sessionStorage.removeItem(payload.name)
        return
      }

      channel.postMessage({ action: 'SET', name: payload.name, data })
    }],
    ['_STORE_SESSION', (payload) => {
      window.sessionStorage.setItem(payload.name, payload.data)
    }],
    ['_LOGOUT', async () => {
      await useApi('/api/auth/logout', { method: 'POST' })
      window.location.reload()
    }],
    ['_REFRESH', () => {
      refreshNuxtData()
    }],
    ['_RELOAD', () => {
      window.location.reload()
    }]
  ])

  channel.onmessage = ({ data }) => {
    app.$log('on message:', data)
    switch (data.action) {
      case 'GET':
        return actions.get('_RETRIEVE_SESSION')(data)
      case 'SET':
        return actions.get('_STORE_SESSION')(data)
      case 'LOGOUT':
        return actions.get('_LOGOUT')()
      case 'REFRESH':
        return actions.get('_REFRESH')()
      case 'RELOAD':
        return actions.get('_RELOAD')()
    }
  }

  return channel
}
