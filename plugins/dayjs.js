import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime.js'
import updateLocale from 'dayjs/plugin/updateLocale.js'
import isBetween from 'dayjs/plugin/isBetween.js'
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter.js'

dayjs.extend(relativeTime, {
  thresholds: [
    { l: 's', r: 1 },
    { l: 'm', r: 1 },
    { l: 'mm', r: 59, d: 'minute' },
    { l: 'h', r: 1 },
    { l: 'hh', r: 23, d: 'hour' },
    { l: 'd', r: 1 },
    { l: 'dd', r: 29, d: 'day' },
    { l: 'M', r: 1 },
    { l: 'MM', r: 11, d: 'month' },
    { l: 'y', r: 1 },
    { l: 'yy', d: 'year' }
  ]
})
dayjs.extend(updateLocale)
dayjs.extend(isBetween)
dayjs.extend(isSameOrAfter)

dayjs.updateLocale('en', {
  relativeTime: {
    future: '%s後',
    past: '%s前',
    s: '幾秒',
    m: '1分鐘',
    mm: '%d分鐘',
    h: '1小時',
    hh: '%d小時',
    d: '1天',
    dd: '%d天',
    M: '1個月',
    MM: '%d個月',
    y: '1年',
    yy: '%d年'
  }
})

export default defineNuxtPlugin(() => {
  return {
    provide: {
      time: (string) => dayjs(string).fromNow(),
      timeFormat: (string) => dayjs(string).format('YYYY/MM/DD HH:mm')
    }
  }
})
