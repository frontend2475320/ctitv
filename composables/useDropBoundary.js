import { useElementBounding, useElementSize, useWindowSize } from '@vueuse/core'

export function useDropBoundary ({ target, el }) {
  const { left, top, right, bottom, width, height, update } = useElementBounding(target)
  const { width: ew, height: eh } = useElementSize(el)
  const { width: vw, height: vh } = useWindowSize()
  const status = computed(() => {
    return {
      top: top.value < eh.value,
      bottom: vh.value - bottom.value < eh.value,
      left: (left.value + width.value / 2) < ew.value,
      right: (vw.value - right.value) < ew.value
    }
  })

  const bound = computed(() => {
    return Object.entries(status.value).filter(([key, value]) => value === true).map(([key]) => key)
  })

  return {
    left,
    top,
    right,
    bottom,
    width,
    height,
    ew,
    eh,
    vh,
    vw,
    bound,
    status,
    update
  }
}
