import { H3Event } from 'h3'

interface ResponseTypes {
  error: boolean,
  code: string
  data: unknown | null | any
}

interface ResponseTypesFromApi {
  data: {
    data: any
    code: string
  }
  code: string
}

interface reqWithTokenType {
  (
    path: string,
    token: string,
    payload?: Object,
    timeout?: number
  ): Promise<ResponseTypes>
}

interface reqWithCookeType {
  (
    path: string,
    event: H3Event
  ): Promise<ResponseTypes>
}

export const reqWithToken: reqWithTokenType = async (
  path,
  token,
  payload,
  timeout = 5000
) => {
  const { API_BASE, ENV } = useRuntimeConfig().public
  const headers: HeadersInit = {
    Authorization: `Bearer ${token}`
  }

  if (ENV !== 'prod') {
    headers['User-Agent'] = 'ctinews-ap'
  }

  const response: ResponseTypes = {
    error: false,
    code: '',
    data: null
  }

  // handle timeout
  const controller = new AbortController()
  const timeoutId = setTimeout(() => controller.abort(), timeout)

  try {
    const res: ResponseTypesFromApi = await $fetch(
      path,
      {
        baseURL: API_BASE,
        ...payload,
        headers,
        signal: controller.signal
      }
    )

    response.data = res.data ?? res
    response.code = res.code
  } catch (err) {
    const error = err as ResponseTypesFromApi
    console.log('fetch with token error: ', path, error.data)

    response.error = true
    response.code = error.data?.code || '500'
    response.data = error.data?.data
  }

  clearTimeout(timeoutId)

  return response
}

export const reqWithCookie: reqWithCookeType = async (path, event) => {
  const { API_BASE } = useRuntimeConfig().public

  const response: ResponseTypes = {
    error: false,
    code: '',
    data: null
  }

  try {
    const res = await $fetch.raw(path, { baseURL: API_BASE })
    const cookies = (res.headers.get('set-cookie') || '').split(',')

    for (const cookie of cookies) {
      appendHeader(event, 'set-cookie', cookie)
    }

    const responseData = res._data as ResponseTypesFromApi

    response.data = responseData.data ?? res
    response.code = responseData.code
  } catch (err) {
    const error = err as ResponseTypesFromApi
    console.log('fetch with token error: ', path, error.data)

    response.error = true
    response.code = error.data?.code || '500'
    response.data = error.data?.data
  }

  return response
}
