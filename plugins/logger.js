export default defineNuxtPlugin(app => {
  return {
    provide: {
      log (log) {
        if (app.$config.ENV !== 'prod' || process.env.NODE_ENV === 'development') {
          console.log(...arguments)
        }
      }
    }
  }
})
