import { reqWithToken } from '~~/common/request'

const ELECTION = {
  2022: { id: 'VGQ1m51njZ' },
  2014: { id: '4NQW9mWgXA' },
  2018: { id: 'Dy0rgDWJ9n' }
}

export default defineEventHandler(async (event) => {
  const token = getCookie(event, 'api-token')
  const { city, area, electionType, year } = getQuery(event)
  const election = ELECTION[year]

  const { data, error, code } = await reqWithToken(`/election/candidates/${election.id}`,
    token,
    {
      params: {
        county_name: city,
        town_name: area,
        election_type: electionType
      }
    }
  )

  if (error) {
    throw createError({ statusCode: 500, statusMessage: code, message: `${code}: ${data}` })
  }

  return data
})
