import path from 'path'
import dotenv from 'dotenv'
import components from 'unplugin-vue-components/vite'
import autoImport from 'unplugin-auto-import/vite'
import { VarletUIResolver } from 'unplugin-vue-components/resolvers'

const resolve = (dir: string) => path.join(__dirname, '.', dir)

// load corresponding env file
dotenv.config({ path: resolve(`.env.${process.env.ENV}`) })

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  modules: [
    '@nuxtjs/tailwindcss',
    '@pinia/nuxt',
    '@vueuse/nuxt'
  ],
  runtimeConfig: {
    public: {
      ENV: process.env.ENV,
      BASE_URL: process.env.BASE_URL,
      API_BASE: process.env.API_BASE,
      API_ORIGIN: process.env.API_ORIGIN,
      MAIN_ORIGIN: process.env.MAIN_ORIGIN,
      STORAGE_BASE: process.env.STORAGE_BASE,
      SESSION_DOMAIN: process.env.SESSION_DOMAIN,
      SOCKET_SERVER: process.env.SOCKET_SERVER
    }
  },
  css: [
    '@/assets/style/animation.scss'
  ],
  vite: {
    ssr: {
      noExternal: ['@varlet/ui']
    },
    plugins: [
      components({
        resolvers: [VarletUIResolver()]
      }),
      autoImport({
        resolvers: [VarletUIResolver({ autoImport: true })]
      })
    ],
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@use "@/assets/style/abstract.scss" as *;'
        }
      }
    }
  },
  tailwindcss: {
    cssPath: '@/assets/style/tailwind.scss'
  }
})
