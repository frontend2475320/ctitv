import { reqWithToken } from '~~/common/request'

export default defineEventHandler(async (event) => {
  const token = getCookie(event, 'api-token')
  const { type = 0, page = 1, county = '', town = '' } = getQuery(event)
  const electionId = 'VGQ1m51njZ' // 寫死 2022

  const { data, error, code } = await reqWithToken(
    `/election/popularity/${electionId}`,
    token,
    {
      params: {
        election_type: type,
        page,
        county_name: county,
        town_name: town
      }
    }
  )

  if (error) {
    throw createError({ statusCode: 500, statusMessage: code, message: `${code}: ${data}` })
  }

  return data
})
