import { useToast } from '~~/common/shared-state'

export function useToastNotify (id) {
  const toasts = useToast()
  let timeout = null

  onMounted(() => {
    timeout = setTimeout(removeToast, 3000)
  })

  onBeforeUnmount(() => {
    if (timeout) {
      clearTimeout(timeout)
      timeout = null
    }
  })

  function removeToast () {
    toasts.value.delete(id)
  }

  return {
    remove: removeToast
  }
}
