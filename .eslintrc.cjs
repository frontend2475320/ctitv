module.exports = {
  root: true,
  extends: [
    'plugin:vue/vue3-recommended',
    '@nuxt/eslint-config',
    'standard'
  ],
  env: {
    browser: true,
    es2020: true,
    node: true
  },
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  rules: {
    'vue/multi-word-component-names': 'off',
    'no-undef': 'off'
  }
}
