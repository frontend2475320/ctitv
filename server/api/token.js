import { reqWithCookie } from '~~/common/request'

export default defineEventHandler(async (event) => {
  const { SESSION_DOMAIN } = useRuntimeConfig().public

  const { data: token, code, error } = await reqWithCookie('/token', event)

  if (error) {
    throw createError({ statusCode: 500, statusMessage: code, message: `${code}: ${data}` })
  }

  setCookie(event, 'api-token', token, { domain: SESSION_DOMAIN })
  console.log('===> set token: ', token)
  // appendHeader(event, 'set-cookie', `api-token=${token};Domain=${SESSION_DOMAIN}`)

  return {
    token
  }
})
