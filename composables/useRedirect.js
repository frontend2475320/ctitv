// 設定API導轉出去跳轉回來的url

export function useRedirect () {
  const { BASE_URL, SESSION_DOMAIN } = useRuntimeConfig().public
  const { $log } = useNuxtApp()
  const cookie = useCookie('redirec-uri', { domain: SESSION_DOMAIN })

  function setRedirect (path) {
    const redirectURL = BASE_URL.concat(path)
    cookie.value = redirectURL
    $log('set redirect: ', redirectURL)
  }

  return {
    url: cookie,
    setRedirect
  }
}
