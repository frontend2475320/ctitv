export function generateHashKey (len = 10) {
  const arr = new Uint8Array(len / 2)
  window.crypto.getRandomValues(arr)
  return Array.from(arr, byteToHex).join('')
}

function byteToHex (byte) {
  return ('0' + byte.toString(16)).slice(-2)
}
