import { nanoid } from 'nanoid'
import { useToast } from '~~/common/shared-state'

export default defineNuxtPlugin(app => {
  return {
    provide: {
      toast (data) {
        const toasts = useToast()
        const uid = nanoid()
        toasts.value.set(uid, { id: uid, ...data })
      }
    }
  }
})
