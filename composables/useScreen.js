import { reactive } from 'vue'
import { useMediaQuery, useWindowScroll, useWindowSize } from '@vueuse/core'

export default function useScreen () {
  const { y: scrollTop } = useWindowScroll()
  const { width: vw, height: vh } = useWindowSize({ initialHeight: 375, initialWidth: 375 })

  const screen = reactive({
    '2xl': useMediaQuery('(max-width: 1536px)'),
    xl: useMediaQuery('(max-width: 1280px)'),
    lg: useMediaQuery('(max-width: 1024px)'),
    md: useMediaQuery('(max-width: 768px)'),
    sm: useMediaQuery('(max-width: 640px)'),
    xs: useMediaQuery('(max-width: 480px)'),
    vw,
    vh,
    scrollTop
  })

  return screen
}
