import { reqWithToken } from '~~/common/request'

export default defineEventHandler(async (event) => {
  const token = getCookie(event, 'api-token')
  const { hash } = event.context.params
  const body = await readBody(event)

  const { data, error, code } = await reqWithToken(
    `/member/reset-password/${hash}`,
    token,
    {
      method: 'POST',
      body
    }
  )

  if (error) {
    throw createError({ statusCode: 500, statusMessage: code, message: `${code}: ${data}` })
  }

  return !data
})
