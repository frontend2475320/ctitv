import { reqWithToken } from '~~/common/request'

export default defineEventHandler(async (event) => {
  const token = getCookie(event, 'api-token')
  const { id } = event.context.params

  const { data, error, code } = await reqWithToken(`/election/candidate/${id}`, token)

  if (error) {
    throw createError({ statusCode: 500, statusMessage: code, message: `${code}: ${data}` })
  }

  return { candidate: data }
})
