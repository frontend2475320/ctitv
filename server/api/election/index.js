import { awaitAllSettled } from '@/utils/promise'
import { reqWithToken } from '~~/common/request'

export default defineEventHandler(async (event) => {
  const token = getCookie(event, 'api-token')
  const response = await awaitAllSettled([
    reqWithToken('/election/leaderboard/list', token),
    reqWithToken('/election/poll/list', token)
  ])

  if (response.some(({ error }) => error)) {
    throw createError({ statusCode: 500, statusMessage: '500', message: 'API Error' })
  }

  const [
    { data: leaderBoard = [] },
    { data: polls = [] }
  ] = response

  return {
    election: {
      leaderBoard,
      polls
    }
  }
})
