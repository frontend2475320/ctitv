import { reqWithToken } from '~~/common/request'

export default defineEventHandler(async (event) => {
  const token = getCookie(event, 'api-token')
  const body = await readBody(event)

  const { data, error, code } = await reqWithToken(
    '/member/register',
    token,
    {
      method: 'POST',
      body
    }
  )

  if (error) {
    throw createError({ statusCode: 500, statusMessage: code, message: `${code}: ${JSON.stringify(data)}` })
  }

  return data
})
