import { defineStore } from 'pinia'

export const useGroupStore = defineStore({
  id: 'group',
  state: () => {
    return {
      groups: [
        {
          code: 'tsp',
          name: '台灣基進',
          color: {
            main: '#BC482A',
            second: '#C45F41',
            third: '#CC755A',
            fourth: '#D48B75',
            fifth: '#DCA290',
            sixth: '#E5B9AB'
          },
          logo: '@/assets/svg/group/tsp.svg'
        },
        {
          code: 'tpp',
          name: '台灣民眾黨',
          color: {
            main: '#28C8C8',
            second: '#6BD5D6',
            third: '#7ADADC',
            fourth: '#8DE0E2',
            fifth: '#A2E6E7',
            sixth: '#B8ECED'
          },
          logo: '@/assets/svg/group/tpp.svg'
        },
        {
          code: 'kmt',
          name: '國民黨',
          color: {
            main: '#0073B8',
            second: '#1288E8',
            third: '#27A2FC',
            fourth: '#54B9FD',
            fifth: '#92D6FF',
            sixth: '#C2E8FE'
          },
          logo: '@/assets/svg/group/kmt.svg'
        },
        {
          code: 'topep',
          name: '小民參政歐巴桑聯盟',
          color: {
            main: '#915920',
            second: '#AA7037',
            third: '#C88643',
            fourth: '#D09A60',
            fifth: '#DAAD7F',
            sixth: '#E3C19E'
          },
          logo: '@/assets/svg/group/topep.svg'
        },
        {
          code: 'np',
          name: '新黨',
          color: {
            main: '#FFDB00',
            second: '#FBDF52',
            third: '#FCE361',
            fourth: '#FCE876',
            fifth: '#FDED8F',
            sixth: '#FDF1A9'
          },
          logo: '@/assets/svg/group/np.svg'
        },
        {
          code: 'npp',
          name: '時代力量',
          color: {
            main: '#FFA000',
            second: '#FEA732',
            third: '#FEB952',
            fourth: '#FECA79',
            fifth: '#FFDBA3',
            sixth: '#FFEDD0'
          },
          logo: '@/assets/svg/group/npp.svg'
        },
        {
          code: 'dpp',
          name: '民進黨',
          color: {
            main: '#2E7D32',
            second: '#359E41',
            third: '#4FBC5B',
            fourth: '#77CC80',
            fifth: '#A2DEA8',
            sixth: '#D0EED3'
          },
          logo: '@/assets/svg/group/dpp.svg'
        },
        {
          code: 'none',
          name: '無黨籍',
          color: {
            main: '#373B41',
            second: '#4B5059',
            third: '#68707B',
            fourth: '#8A919C',
            fifth: '#B0B5BB',
            sixth: '#D7D9DC'
          },
          logo: '@/assets/svg/group/none.svg'
        },
        {
          code: 'sdp',
          name: '社會民主黨',
          color: {
            main: '#FF0088',
            second: '#EC4095',
            third: '#ED53A3',
            fourth: '#EE6DB2',
            fifth: '#F088C1',
            sixth: '#F3A4D0'
          },
          logo: '@/assets/svg/group/sdp.svg'
        },
        {
          code: 'gp',
          name: '綠黨',
          color: {
            main: '#91D74D',
            second: '#ADDA75',
            third: '#BAE08B',
            fourth: '#C8E6A1',
            fifth: '#D6ECB9',
            sixth: '#E4F2D0'
          },
          logo: '@/assets/svg/group/gp.svg'
        },
        {
          code: 'pfp',
          name: '親民黨',
          color: {
            main: '#FF6310',
            second: '#F08045',
            third: '#F19560',
            fourth: '#F3A97E',
            fifth: '#F6BE9E',
            sixth: '#F9D4BE'
          },
          logo: '@/assets/svg/group/pfp.svg'
        },
        {
          code: 'gpp',
          name: '金色力量黨',
          color: {
            main: '#ECAC24',
            second: '#E7B959',
            third: '#EBC572',
            fourth: '#EFD08C',
            fifth: '#F3DBA8',
            sixth: '#F7E7C4'
          },
          logo: '@/assets/svg/group/pfp.svg'
        }
      ]
    }
  },
  actions: {
    async fetch () {
      const { data } = await useApi('/api/election/group')
      this.groups = data.value
    }
  },
  getters: {
    getGroupById (id) {
      return this.groups.find(group => group.id === id)
    }
  }
})
