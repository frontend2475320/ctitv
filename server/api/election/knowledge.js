import { reqWithToken } from '~~/common/request'

export default defineEventHandler(async (event) => {
  const token = getCookie(event, 'api-token')
  const electionId = 'VGQ1m51njZ' // 寫死 2022

  const { data, error, code } = await reqWithToken(`/election/knowledge/${electionId}`, token)

  if (error) {
    throw createError({ statusCode: 500, statusMessage: code, message: `${code}: ${data}` })
  }

  return data
})
