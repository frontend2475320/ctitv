import { defineStore } from 'pinia'

const proxy = ({ data, city }) => {
  const areas = []

  if (!data || !Array.isArray(data)) {
    return areas
  }

  data.forEach(area => {
    area.towns.forEach(town => {
      areas.push({ area: area.name, region: `${city}${town.town_name}`, city, towns: area.towns })
    })
  })

  return areas
}

export const useAreaStore = defineStore({
  id: 'area',
  state: () => {
    return {
      areas: []
    }
  },
  actions: {
    async fetch (city) {
      if (this.areas.findIndex(electionArea => electionArea.city === city) === -1) {
        const { data } = await useApi('/api/election/area/list', { params: { city } })
        const electionAreas = proxy({ data: data.value, city })

        electionAreas.forEach(area => {
          this.areas.push(area)
        })

        // 新竹市的地圖與選區的資料對不起來，所以額外將新竹市底下的行政區都視為同一個選區
        this.areas.forEach((area, areaIdx) => {
          if (area.city === '新竹市') {
            this.areas[areaIdx].area = '新竹市選區'
            this.areas[areaIdx].towns = [
              { town_name: '北區' },
              { town_name: '香山區' },
              { town_name: '東區' }
            ]
          }
        })
      }
    }
  }
})
