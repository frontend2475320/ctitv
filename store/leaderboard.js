import { defineStore } from 'pinia'

export const useLeaderBoardStore = defineStore({
  id: 'leaderboard',
  state: () => ({
    id: 'VGQ1m51njZ', // 寫死 2022
    type: '0',
    pending: false,
    page: 0,
    totalPages: 1,
    total: 0,
    countyName: '',
    townName: '',
    regionName: '',
    items: []
  }),
  getters: {
    completed: state => state.page >= state.totalPages
  },
  actions: {
    init ({ type, countyName = '', townName = '' } = {}) {
      this.items = []
      this.page = 0
      this.type = type
      this.countyName = countyName
      this.townName = townName

      this.load()
    },
    async fetch ({ page = 1 } = {}) {
      if (this.pending) {
        return
      }
      this.pending = true

      const { data, error } = await useApi(
        '/api/election/leaderboard',
        { params: { page, type: this.type, county: this.countyName, town: this.townName } }
      )

      this.pending = false
      if (error.value) {
        return
      }

      this.page = data.value.page
      this.totalPages = data.value.pages
      this.total = data.value.total

      return data.value
    },
    async load (payload = {}) {
      if (this.completed) {
        return
      }
      const { items = [] } = await this.fetch({ page: this.page + 1, ...payload }) || {}

      this.items = this.items.concat(items)
    }
  }
})
