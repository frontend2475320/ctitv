import { reqWithToken } from '~~/common/request'

export default defineEventHandler(async (event) => {
  const token = getCookie(event, 'api-token')
  const { type, id } = event.context.params
  const { sort = 1, page = 1 } = getQuery(event)

  if (!(type && id)) throw new Error('missing params')

  const { data, error, code } = await reqWithToken(
    `/comment/list/${type}/${id}`,
    token,
    {
      params: {
        sort,
        page
      }
    }
  )

  if (error) {
    throw createError({ statusCode: 500, statusMessage: code, message: `${code}: ${data}` })
  }
  return data
})
