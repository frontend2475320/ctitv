export const ELECTION = Object.freeze({
  TYPE: { 1: '縣市長', 2: '縣市議員', 3: '鄉鎮區長', 4: '里長', 5: '總統' },
  SUFFIX: { 1: '長候選人', 2: '議員候選人', 3: '長候選人', 4: '長候選人', 5: '總統候選人' },
  REGION: {
    MAYOR: {
      1: '六都',
      2: '其他縣市'
    },
    COUNCILOR: {
      3: '北部',
      4: '中部',
      5: '南部',
      6: '東部',
      7: '離島'
    },
    KEY: {
      8: '台北市士林區天玉里',
      9: '桃園市桃園區春日里'
    }
  }
})

export const REACTION = Object.freeze({
  LIKE: 1,
  DISLIKE: 2,
  UNLIKE: 3,
  UNDISLIKE: 4
})
