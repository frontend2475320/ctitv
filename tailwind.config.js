import plugin from 'tailwindcss/plugin'

export default {
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      primary: '#0073B8',
      theme: {
        2: '#0089DB',
        3: '#00C2FF'
      },
      black: '#222323',
      white: '#fff',
      blue: {
        100: '#F1F6FF',
        300: '#CBEBFF',
        500: '#005BEA',
        700: '#0170C0',
        800: '#415072',
        900: '#376699'
      },
      gray: {
        10: '#F1F2ED',
        50: '#E0E0E0',
        80: '#D8D8D8',
        100: '#C8C8C8',
        200: '#C0C1C2',
        300: '#ACACAC',
        350: '#959697',
        400: '#828282',
        450: '#71798C',
        500: '#424242',
        900: '#222323',
        950: '#0D1C2E',
        960: '#151A1D'
      },
      red: {
        300: '#FF054C',
        DEFAULT: '#E63F29',
        500: '#E63F29',
        700: '#C81E1E'
      },
      orange: {
        100: '#FFEDE3',
        150: '#FF7961',
        300: '#F4895A',
        500: '#F28706',
        900: '#8B3017'
      },
      pink: {
        100: '#FFF2F6',
        500: '#FF054C'
      },
      yellow: {
        400: '#FFC93F',
        500: '#FAD37C',
        700: '#C58E15',
        900: '#724900'
      },
      purple: {
        100: '#E2E1FF',
        500: '#5B2575'
      }
    },
    fontSize: {
      xs: ['0.75rem'],
      sm: ['0.875rem'],
      base: ['1rem'],
      lg: ['1.125rem'],
      xl: ['1.25rem'],
      '2xl': ['1.5rem'],
      '3xl': ['1.625rem'],
      '4xl': ['1.75rem'],
      '5xl': ['2rem', { lineHeight: '1.25' }],
      '6xl': ['2.25rem'],
      '7xl': ['2.875rem', { lineHeight: '1.3' }]
    },
    fontFamily: {
      base: ['Helvetica', 'Noto Sans TC', 'sans-serif'],
      icon: ['Material Icons Round', 'sans-serif']
    },
    borderRadius: {
      none: '0px',
      xs: '0.25rem',
      sm: '0.375rem',
      DEFAULT: '0.5rem',
      md: '0.625rem',
      full: '9999px',
      lg: '0.75rem',
      xl: '1rem'
    },
    screens: {
      '2xl': { max: '1536px' },
      xl: { max: '1280px' },
      lg: { max: '1024px' },
      md: { max: '768px' },
      sm: { max: '640px' },
      xs: { max: '480px' }
    },
    extend: {
      zIndex: Array.from({ length: 5 }).reduce((result, o, i) => {
        result[i + 1] = i + 1
        return result
      }, {})
    }
  },
  plugins: [
    plugin(({ addVariant }) => {
      addVariant('em', ({ container }) => {
        container.walkRules(rule => {
          rule.selector = `.em\\:${rule.selector.slice(1)}`
          rule.walkDecls((decl) => {
            decl.value = decl.value.replace('rem', 'em')
          })
        })
      })
    })
  ]
}
