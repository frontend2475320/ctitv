module.exports = {
  extends: [
    'stylelint-config-standard-scss',
    'stylelint-config-recommended-vue',
    'stylelint-config-recess-order'
  ],
  rules: {
    'function-no-unknown': null,
    'at-rule-no-unknown': null,
    'scss/at-rule-no-unknown': null,
    'selector-class-pattern': null,
    'color-function-notation': null,
    'alpha-value-notation': 'number',
    'string-quotes': 'single',
    'custom-property-no-missing-var-function': null,
    'value-keyword-case': null,
    'keyframes-name-pattern': null
  },
  ignoreFiles: [
    '.vscode/**',
    'dist/**',
    'node_modules/**',
    'assets/style/vendor/**'
  ]
}
