import { defineStore } from 'pinia'
import { REACTION } from '@/common/enum'

const REPORT_TYPE = {
  1: '廣告/垃圾訊息',
  2: '內容不實或具威脅性',
  3: '色情暴力',
  4: '惡性洗版',
  5: '其他原因'
}

export const useCommentStore = defineStore({
  id: 'comment',
  state: () => ({
    id: '',
    type: '',
    sort: 1,
    pending: false,
    comments: [],
    page: 0,
    totalPages: 1,
    total: 0,
    position: {
      pros: '',
      cons: ''
    }
  }),
  getters: {
    completed: state => state.page >= state.totalPages
  },
  actions: {
    async init ({ type, id, sort }) {
      this.id = id
      this.type = type
      this.sort = sort

      await this.load()
    },
    async fetch ({ sort = 1, page = 1 } = {}) {
      if (this.pending) return

      this.pending = true
      const { data, error } = await useApi(
        `/api/comment/list/${this.type}/${this.id}`,
        { params: { sort, page } }
      )
      this.pending = false

      if (error.value) return

      this.sort = sort
      this.page = data.value.page
      this.totalPages = data.value.pages
      this.total = data.value.total

      return data.value
    },
    async load (payload = {}) {
      if (this.completed) return
      let { comments = [] } = await this.fetch({ sort: this.sort, page: this.page + 1, ...payload }) || {}

      if (this.type === 'poll' && this.position.pros) {
        comments = comments.map(o => {
          const position = Object.entries(this.position).find(
            ([p, v]) => o.poll_ansers[0].id === v
          )

          return {
            ...o,
            position: position?.[0] || 'neutral'
          }
        })
      }

      this.comments = this.comments.concat(comments)
    },
    async refresh (payload = {}) {
      this.sort = 1
      this.comments = []
      this.page = 0
      this.totalPages = 1
      this.total = 0

      this.load(payload)
    },
    async add (data) {
      const { $toast, $log } = useNuxtApp()

      this.pending = true
      const { error, data: resData } = await useApi(
        `/api/comment/add/${this.type}/${this.id}`,
        {
          method: 'POST',
          body: {
            is_guest: data.anonymous,
            text: data.msg
          }
        }
      )
      this.pending = false
      $log('add: ', resData.value)

      if (!error.value) {
        // successed
        await this.refresh()
        $toast({
          type: 'info',
          text: '新增留言成功'
        })
      } else {
        // failed
        $toast({
          type: 'alert',
          text: error.value.data.message || '新增留言失敗'
        })

        throw new Error('failed')
      }
    },
    async delete (id) {
      const { $toast } = useNuxtApp()

      this.pending = true
      const { error } = await useApi(
        `/api/comment/delete/${id}`,
        { method: 'DELETE' }
      )
      this.pending = false

      if (!error.value) {
        // successed
        const index = this.comments.findIndex(o => o.id === id)
        this.comments.splice(index, 1)
        this.refresh()
        $toast({
          type: 'info',
          text: '已刪除留言'
        })
      } else {
        // failed
        $toast({
          type: 'alert',
          text: error.value.data.message || '操作失敗'
        })

        throw new Error('failed')
      }
    },
    async edit (id, data) {
      const { $toast } = useNuxtApp()
      this.pending = true
      const { error } = await useApi(
        `/api/comment/edit/${id}`,
        {
          method: 'PUT',
          body: {
            text: data
          }
        }
      )
      this.pending = false

      if (!error.value) {
        const theComment = this.comments.find(o => o.id === id)
        theComment.text = data
        // successed
        $toast({
          type: 'info',
          text: '已修改留言'
        })
      } else {
        // failed
        $toast({
          type: 'alert',
          text: error.value.data.message || '操作失敗'
        })

        throw new Error('failed')
      }
    },
    async report (id, formData) {
      const { $toast } = useNuxtApp()
      const type = Object.entries(REPORT_TYPE).find(o => o.includes(formData.reason))[0]

      this.pending = true
      const { error } = await useApi(
        `/api/comment/report/${id}`,
        {
          method: 'POST',
          body: {
            reason_type: type,
            reason_text: formData.text || formData.reason
          }
        }
      )
      this.pending = false

      if (!error.value) {
        // successed
        $toast({
          type: 'info',
          text: '已檢舉留言'
        })
      } else {
        // failed
        $toast({
          type: 'alert',
          text: error.value.data.message || '操作失敗'
        })

        throw new Error('failed')
      }
    },
    async react (id, type) {
      const { $toast, $log } = useNuxtApp()

      $log(id, { feel_type: type })

      this.pending = true
      const { error } = await useApi(
        `/api/comment/react/${id}`,
        {
          method: 'POST',
          body: {
            feel_type: type
          }
        }
      )
      this.pending = false

      if (error.value) {
        console.log($toast, 'error')
        $toast({
          type: 'alert',
          text: error.value.data.message || '操作失敗'
        })

        throw new Error('failed')
      }

      // @type 1: 按讚, 2:倒讚, 3:收回讚, 4:收回倒讚
      const theComment = this.comments.find(o => o.id === id)

      switch (type) {
        case REACTION.LIKE: {
          theComment.like++
          theComment.is_like = true

          if (theComment.is_dislike) {
            theComment.dislike--
            theComment.is_dislike = false
          }
          break
        }
        case REACTION.DISLIKE: {
          theComment.dislike++
          theComment.is_dislike = true

          if (theComment.is_like) {
            theComment.like--
            theComment.is_like = false
          }
          break
        }
        case REACTION.UNLIKE: {
          theComment.like--
          theComment.is_like = false
          break
        }
        case REACTION.UNDISLIKE: {
          theComment.dislike--
          theComment.is_dislike = false
          break
        }
      }
    }
  }
})
