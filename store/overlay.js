import { defineStore } from 'pinia'

export const useOverlayStateStore = defineStore({
  id: 'overlayState',
  state: () => {
    return {
      isOpenOverlay: false
    }
  },
  getters: {
    isActive: state => state.isOpenOverlay
  },
  actions: {
    open () {
      this.isOpenOverlay = true
    },
    close () {
      this.isOpenOverlay = false
    }
  }
})
