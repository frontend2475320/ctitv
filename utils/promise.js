export async function awaitAllSettled (promises) {
  const responses = await Promise.allSettled(promises)
  return responses.map(o => ({ ...o.value }))
}
