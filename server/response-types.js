export const TOKEN = {
  '0000': 'SUCCESS',
  1079: 'RETRIEVE_TOKEN',
  1078: 'RETRIEVE_TOKEN'
}

export const LOGIN = {
  2205: 'INVALID_FIELD',
  2206: 'INVALID_FIELD'
}

export const SIGNUP = {
  422: 'INVALID_FIELD',
  2203: 'INVALID_CODE'
}

export const FORGOT_PWD = {
  2205: 'UNREGISTERED',
  2211: 'INVALID_TOKEN'
}

export const VIDEO = {
  400: 'VIDEO_NOT_FOUND'
}

export const PROGRAM = {
  400: 'PROGRAM_NOT_FOUND'
}

export const AUDIO = {
  2102: 'NO_DATA'
}
