import { defineStore } from 'pinia'

export const useUserStore = defineStore({
  id: 'user',
  state: () => {
    return {
      token: '',
      tokenStatus: {},
      profile: {},
      notifications: 0,
      cards: [],
      showAd: false
    }
  },
  getters: {
    isLoggedIn: state => Boolean(state.profile.id),
    avatar: state => state.profile.picture,
    coin: state => state.profile.wancoin,
    name: state => state.profile.name,
    level: state => state.profile.type,
    suspended: state => state.profile.status === 0,
    banned: state => state.profile.status === 2,
    unavailable: state => [0, 2].includes(state.profile.status)
  },
  actions: {
    async fetch () {
      const { data, error } = await useApi('/api/user')
      if (!error.value) this.profile = data.value
    }
    // async fetchCardList () {
    //   const { data, error } = await useApi('/api/account/card/list', {}, { initialCache: false })
    //   if (!error.value) this.cards = data.value
    // }
  }
})
