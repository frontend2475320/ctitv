import { reqWithToken } from '~~/common/request'

export default defineEventHandler(async (event) => {
  const token = getCookie(event, 'api-token')
  const { data, error, code } = await reqWithToken('/election/item/list', token)

  if (error) {
    throw createError({ statusCode: 500, statusMessage: code, message: `${code}: ${data}` })
  }

  const current = data.find((item) => item.current)

  return {
    data,
    current
  }
})
