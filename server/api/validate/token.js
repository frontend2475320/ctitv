import { reqWithCookie, reqWithToken } from '~~/common/request'
import { TOKEN as STATUS_CODE } from '@/server/response-types'

export default defineEventHandler(async (event) => {
  const { SESSION_DOMAIN } = useRuntimeConfig().public
  let token = getCookie(event, 'api-token')
  let tokenStatus

  const { data, code: statusCode } = await reqWithToken('/token/detail', token)
  tokenStatus = data

  const needRefreshToken = (STATUS_CODE[statusCode] === 'RETRIEVE_TOKEN') || !data.status
  console.log('need to refresh token', needRefreshToken)

  if (needRefreshToken) {
    // 若cookie沒記錄或token過期 -> 重新獲取token
    const { data, code, error } = await reqWithCookie('/token', event)

    if (error) {
      throw createError({ statusCode: 500, statusMessage: code, message: `${code}: ${data}` })
    }

    token = data
    const { data: refreshedStatus } = await reqWithToken('/token/detail', token)

    tokenStatus = refreshedStatus
    setCookie(event, 'api-token', token, { domain: SESSION_DOMAIN })
    console.log('set token: ', token)
  }

  return {
    token,
    tokenStatus
  }
})
