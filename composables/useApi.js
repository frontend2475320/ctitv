import { useToken } from '@/common/shared-state'

export function useApi (path, payload) {
  const { $log } = useNuxtApp()
  const token = useToken()
  const headers = useRequestHeaders(['cookie'])
  const cookieFromHeader = headers.cookie && getCookie(headers.cookie, 'api-token')
  const apiToken = cookieFromHeader ?? token.value

  $log(`---${path}---`)
  $log(`token from store: ${token.value}`)
  $log(`token from headers: ${cookieFromHeader}`)
  $log('token in use: ', token.value)

  return useFetch(
    path,
    {
      headers: { cookie: `api-token=${apiToken}` },
      ...payload
    }
  )
}

function getCookieList (string) {
  return Object.fromEntries(
    string?.split(' ')[0].replace(';', '').split(',').map((a) => a.split('='))
  )
}

function getCookie (string, name) {
  const cookie = getCookieList(string)
  return cookie[name]
}
